/**
 * @author Ognjen Crvic
 * @date 30-Oct-21
 */
public class Request {
    private Integer currentFloor;
    private Integer destinationFloor;
    private Direction direction;

    public Request(Integer currentFloor, Integer destinationFloor, Direction direction) {
        this.currentFloor = currentFloor;
        this.destinationFloor = destinationFloor;
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "[current floor: " + currentFloor +
                ", destination floor: " + destinationFloor +
                ", direction: " + direction + "]";
    }

    public Integer getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(Integer currentFloor) {
        this.currentFloor = currentFloor;
    }

    public Integer getDestinationFloor() {
        return destinationFloor;
    }

    public void setDestinationFloor(Integer destinationFloor) {
        this.destinationFloor = destinationFloor;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
