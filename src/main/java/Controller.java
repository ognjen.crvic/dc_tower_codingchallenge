import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Ognjen Crvic
 * @date 30-Oct-21
 */
public class Controller {

    private final Elevator[] elevators;
    private final ExecutorService executorService;

    public Controller(int numOfElevators) {
        this.elevators = new Elevator[numOfElevators];
        this.executorService = Executors.newFixedThreadPool(numOfElevators);
    }

    /**
     * Initialize all the elevators that are to be used with default values and assign each elevator
     * an individual thread.
     */
    public void init() {
        System.out.println("[Elevator #]\t" + "Floor\t" + "Dir\t\t " + "PATH\n");

        for (int i = 0; i < elevators.length; i++) {
            elevators[i] = new Elevator(i, Direction.UP, 0, new ArrayList<>() {{
                add(0);
            }});
            executorService.submit(elevators[i]);
        }

    }

    /**
     * Simulates clicking the elevator button by a person
     *
     * @param request object containing information about the trip
     */
    public void addRequest(Request request) {
        Elevator elevator = findClosestElevator(request);
        elevator.addRequest(request);
    }

    /**
     * Finds the closest elevator based on their current position
     *
     * @param request to match with the closes elevator
     * @return elevator that is closest to the caller
     */
    private Elevator findClosestElevator(Request request) {
        return Arrays.stream(elevators)
                .min(Comparator.comparing(elevator -> elevator.distanceTo(request)))
                .get();
    }
}
