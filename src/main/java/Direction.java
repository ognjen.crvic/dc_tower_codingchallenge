/**
 * @author Ognjen Crvic
 * @date 30-Oct-21
 */
public enum Direction {
    UP,
    DOWN
}
