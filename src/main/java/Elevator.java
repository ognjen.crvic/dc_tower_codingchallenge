import java.util.List;

/**
 * @author Ognjen Crvic
 * @date 30-Oct-21
 */
public class Elevator implements Runnable {

    private final Integer id;
    private final int THREAD_DEFAULT_SLEEP_TIME = 1000;
    private Direction direction;
    private Integer currentFloor;
    private final List<Integer> path;
    private boolean busy;

    public Elevator(Integer id, Direction direction, Integer currentFloor, List<Integer> path) {
        this.id = id;
        this.direction = direction;
        this.currentFloor = currentFloor;
        this.path = path;
        this.busy = false;
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(THREAD_DEFAULT_SLEEP_TIME);
                update();
                printInfo();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Simulates movement of a single elevator for one time interval
     */
    private void update() {
        busy = path.size() > 1;

        if (busy) {
            currentFloor = (currentFloor > path.get(1)) ? currentFloor - 1 : currentFloor + 1;
            direction = (currentFloor > path.get(1)) ? Direction.DOWN : Direction.UP;
            if (currentFloor.equals(path.get(1))) path.remove(1);

            this.path.set(0, currentFloor);
        }

    }

    /**
     * Formats the information for the console
     */
    private void printInfo() {
        if (busy)
            System.out.println("[Elevator " + id + "]\t " + currentFloor + "\t\t " + direction + " \t" + printPathToString());
        else
            System.out.println("[Elevator " + id + "]\t " + currentFloor + "\t\t " + " -\t\t" + printPathToString());

    }

    /**
     * Generates a string for a path of the task specified format (e.g. 1->2->11->...)
     *
     * @return string containing the full path of the elevator
     */
    public String printPathToString() {
        StringBuilder pathStr = new StringBuilder(" ");
        for (int i : path) {
            pathStr.append(i).append("->");
        }
        return pathStr.substring(0, pathStr.length() - 2);
    }

    @Override
    public String toString() {
        return "Elevator: id=" + id
                + ", direction=" + direction.toString()
                + ", floor=" + currentFloor
                + ", busy=" + busy
                + ", path=[" + printPathToString() + "]";
    }

    /**
     * Calculates the distance between current position and the position in which the person can be picked up
     *
     * @param request defines reference point from which elevator distance is calculated
     * @return the minimal distance the elevator has to pass to pickup the person
     */
    public Integer distanceTo(Request request) {
        int distance = 0;
        if (!busy) {
            //idle elevator
            distance = (request.getDirection() == Direction.UP) ? request.getDestinationFloor() : request.getCurrentFloor();
            distance = Math.abs(distance - currentFloor);
        } else
            //busy elevator
            for (int i = 0; i < path.size() - 1; i++) {
                int head = path.get(i), tail = path.get(i + 1); //head and tail floors of a single sub-path
                Direction currDirection = currentDirection(head, tail);

                if (currDirection != request.getDirection())
                    distance += Math.abs(tail - head);  //elevator's direction different
                else
                    distance += (isStopInSubPath(head, tail, request)) ?
                            Math.abs(request.getCurrentFloor() - head) : Math.abs(tail - head);
            }
        return distance;
    }

    /**
     * Checks if the request is in-between 2 stops of the path of the same direction
     */
    private boolean isStopInSubPath(int head, int tail, Request request) {
        return ((head < request.getDestinationFloor() && tail > request.getDestinationFloor() && request.getDirection() == Direction.UP) ||
                (head > request.getCurrentFloor() && tail < request.getCurrentFloor() && request.getDirection() == Direction.DOWN));
    }

    /**
     * Adds new request into the path of the elevator
     *
     * @param request new request for the current elevator
     */
    public void addRequest(Request request) {
        if (request.getDirection() == Direction.UP)
            addNewStop(request.getDestinationFloor(), request.getDirection());
        else
            addNewStop(request.getCurrentFloor(), request.getDirection());
    }

    /**
     * Adds new stop to the path based on the direction of the request
     */
    private void addNewStop(Integer stop, Direction direction) {
        int[] interval = findIntervalWithDirection(0, direction);

        while (interval != null) {
            // check if okay sublist
            Integer range = stopIsInRange(stop, path.subList(interval[0], interval[1] + 1));
            if (range != null && !path.get(interval[0] + range).equals(stop)) {
                path.add(interval[0] + range, stop);
                break;
            } else
                interval = findIntervalWithDirection(interval[1], direction);
        }

        if (interval == null) {
            if (!path.get(path.size() - 1).equals(stop)) path.add(stop);
            if (direction == Direction.DOWN) path.add(0);
        }
    }

    /**
     * Finds the position of the stop in the sublist, null if not valid value for the sublist
     *
     * @param stop    new stop floor to be added
     * @param subPath part of path which has the same direction as the original request
     * @return index of the stop in sublist
     */
    private Integer stopIsInRange(Integer stop, List<Integer> subPath) {
        int head = subPath.get(0), tail = subPath.get(subPath.size() - 1);

        if (head <= stop && tail >= stop || head >= stop && tail <= stop) {
            for (int i = 0; i < subPath.size(); i++) {
                Direction currDirection = currentDirection(head, tail);

                if (stop < subPath.get(i) && currDirection == Direction.UP ||
                        stop > subPath.get(i) && currDirection == Direction.DOWN)
                    return i;
            }
            return subPath.size() - 1;
        }
        return null;
    }


    private Direction currentDirection(int start, int end) {
        return (start > end) ? Direction.DOWN : Direction.UP;
    }

    /**
     * Finds the first interval of the same direction as parameter
     *
     * @param offset    moves start boundary
     * @param direction in which the interval should be
     * @return start and end index of the interval
     */
    private int[] findIntervalWithDirection(int offset, Direction direction) {
        if (offset >= path.size() - 1) return null;
        int[] interval = null;    //0 start, 1 end
        boolean cond = direction == Direction.UP;

        for (int i = 1 + offset; i < path.size(); i++) {
            int head = (cond) ? path.get(i - 1) : path.get(i);
            int tail = (cond) ? path.get(i) : path.get(i - 1);

            if (head < tail) {
                interval = new int[2];
                interval[0] = i - 1;
                while (head < tail && i < path.size() - 1) {
                    head = (cond) ? path.get(i++) : path.get(++i);
                    tail = (cond) ? path.get(i) : path.get(i - 1);
                }

                interval[1] = (head < tail) ? i : i - 1;
                break;
            }
        }
        return interval;
    }

}
