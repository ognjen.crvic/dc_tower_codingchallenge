/**
 * @author Ognjen Crvic
 * @date 30-Oct-21
 */
public class Run {

    public static void main(String[] args) throws InterruptedException {
        Controller controller = new Controller(2);
        controller.init();

        test1(controller);

    }

    public static void test1(Controller controller) throws InterruptedException {

        controller.addRequest(new Request(0, 12, Direction.UP));
        Thread.sleep(2000);
        controller.addRequest(new Request(3, 0, Direction.DOWN));


        controller.addRequest(new Request(12, 0, Direction.DOWN));
        Thread.sleep(5000);

        controller.addRequest(new Request(11, 0, Direction.DOWN));
        Thread.sleep(2000);
        controller.addRequest(new Request(8, 0, Direction.DOWN));

        controller.addRequest(new Request(0, 5, Direction.UP));

    }

    public static void test2(Controller controller) throws InterruptedException {

        controller.addRequest(new Request(0, 5, Direction.UP));
        Thread.sleep(10000);

        controller.addRequest(new Request(6, 0, Direction.DOWN));
        Thread.sleep(5000);

        controller.addRequest(new Request(11, 0, Direction.DOWN));
        Thread.sleep(2000);

        controller.addRequest(new Request(5, 0, Direction.DOWN));

    }

    public static void test3(Controller controller) throws InterruptedException {

        controller.addRequest(new Request(5, 0, Direction.DOWN));
        Thread.sleep(2000);

        controller.addRequest(new Request(6, 0, Direction.DOWN));
        Thread.sleep(3000);

        controller.addRequest(new Request(11, 0, Direction.DOWN));
        Thread.sleep(2000);

        controller.addRequest(new Request(0, 5, Direction.UP));

    }
}
