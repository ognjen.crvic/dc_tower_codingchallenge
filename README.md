# DC_Tower_CodingChallenge

## Controller Class

- Contains a record of all elevators and manages their tasks (adds tasks to the closest elevator).
- Runs each elevator as a separate thread.

## Request Class

- Coupling of all request attributes, used to simulate information by clicking on the elevator button.

## Elevator Class

- Represents a single elevator instance 
- implements main logic of the system. 
    - calculates distance for the closest available pickup
    - adds new stop to the first trip in the same direction

## Run Class

- Class used for running/testing the system.
- Multiple tests available (test1, test2, test3) to test various situations.
- Outputs the following content:
```
[Elevator #]	Floor           Dir      PATH

[Elevator 1]	 0		  -      0
[Elevator 0]	 1		 UP 	 1->12
[Elevator 1]	 0		 UP      1->3->0
[Elevator 0]	 2		 UP 	 2->12->0
[Elevator 1]	 0		 UP      2->3->0
[Elevator 0]	 3		 UP 	 3->12->0
[Elevator 1]	 0		 UP      3->0
[Elevator 0]	 4		 UP 	 4->12->0
[Elevator 1]	 0	        DOWN     2->0
[Elevator 0]	 5		 UP 	 5->12->0
```

